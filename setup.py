import setuptools

setuptools.setup(
    name="acccamt",
    version="0.1.0",
    author="Genossenschaft Solutionsbüro",
    author_email="msg@frg72.com",
    packages=setuptools.find_packages(),
    include_package_data=True,
    package_data={"": ["camt.053.001.04.xsd"]},
)
