# acccamt

Import Bank to customer statements from a bank-account using ISO 20022 camt.053.001.04 (`BankToCustomerStatementV04`). [Reference](https://www.six-group.com/dam/download/banking-services/interbank-clearing/en/standardization/iso/swiss-recommendations/implementation-guidelines-camt.pdf), [XML-Schema](https://www.six-group.com/dam/download/banking-services/interbank-clearing/en/standardization/iso/swiss-recommendations/camt.053.001.04.zip)

This package comes with **absolutely NO warranty at all!**. This is about your finances so be careful!
