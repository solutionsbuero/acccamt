"""
Module containing the XML tags as constants.
"""

BANK_TO_CUSTOMER_STATEMENT = "BkToCstmrStmt"
"""
The XML message "Bank-to-Customer Statement" (camt.053) is used by financial
institutions to send electronic account information to their customers. It is
used on the basis of the ISO 20022 XML schema "camt.053.001.04".
"""

GROUP_HEADER = "GrpHdr"
"""
The "Group Header" (A-Level of the message) contains information about the
message. It occurs once.
"""

CREATION_DATE_TIME = "CreDtTm"
"""
Date and time when message was created
"""

STATEMENT = "Stmt"
"""
Only one instance will be provided, one account per "camt"
message. Details about the statement for which the following
information is being delivered. This level is described as followed in
the various "camt" messages: camt.054: Notification of credits and debits and
batch booking breakdown
"""

ACCOUNT = "Acct"
"""
Information about the account, its owner and the financial institution.
"""

IDENTIFICATION = "Id"
"""
This element is used as follows:
- IBAN or
- Proprietary Account
(Some financial institutions offer IBAN exclusively.)
"""

IBAN = "IBAN"
"""
If used, then "Proprietary Account" must not be present.
"""

OWNER = "Ownr"
"""
Information about the account holder
"""

NAME = "Nm"
"""Name."""

POSTAL_ADDRESS = "PstlAdr"
"""
Postal address.
"""

ADDRESS_LINE = "AdrLine"
"""
Max. four lines are sent.
This element includes additional information which cannot be
shown in the structured fields (e.g. PO Box).
"""

BUILDING_NUMBER = "BldgNb"
"""
Building number in postal address.
"""

STREET_NAME = "StrtNm"
"""
Street name in postal address.
"""

POST_CODE = "PstCd"
"""
Post code in postal address.
"""

TOWN_NAME = "TwnNm"
"""
Town name in postal address.
"""

COUNTRY = "Ctry"
"""
Country code in postal address.
"""

BANK_TRANSACTION_CODE = "BkTxCd"
"""
BTC consists of 3 fields: Domain, Family and Sub-Family. The following
codes are used:
Credit: Domain = PMNT / Family = RCDT / Sub-Family = VCOM
Return: Domain = PMNT / Family = RCDT / Sub-Family = CAJT
"""

DOMAIN = "Domn"
"""
Domain for the "Bank Transaction Code"
Always sent in Switzerland.
"""

CODE = "Cd"
"""
Domain code for the "Bank Transaction Code"
Always sent in Switzerland.
"""

FAMILY = "Fmly"
"""
Family of the "Bank Transaction Code"
Always sent in Switzerland.
"""

SUB_FAMILY_CODE = "SubFmlyCd"
"""
Sub-family code for the "Bank Transaction Code"
Always sent in Switzerland.
"""

SERVICER = "Svcr"
"""Bank etc."""

FINANCIAL_INSTITUTION_IDENTIFICATION = "FinInstnId"
"""E.g. name of the Bank."""

CURRENCY = "@Ccy"
"""
Account currency.
"""

VALUE_DATE = "ValDt"
"""
Corresponds to the value date.
"""

DATE = "Dt"
"""
Date.
"""

BALANCE = "Bal"
"""
The content of the "camt.053", "camt.052" and "camt.054"
messages differs only in the use of this element. The following
rules apply:
- camt.053: Is always sent.
- camt.052: Can be sent.
- camt.054: Is not sent.
"""

ENTRY = "Ntry"
"""
Detailed information about a single entry
Is always sent, provided at least 1 account movement has taken
place. If there has been no account movement and only account
balances are being reported, this element is not sent.
camt.052/053: This element is optional.
camt.054: This element is always sent.
"""

AMOUNT = "Amt"
"""
Changes to the account balance as a result of all the account movements shown
in the statement.
"""

CREDIT_DEBIT_INDICATOR = "CdtDbtInd"
"""
Shows whether the change (element "Total Net Entry Amount") is positive or
negative.
"""

ENTRY_DETAILS = "NtryDtls"
"""
Contains details about the entry.
"""

TRANSACTION_DETAILS = "TxDtls"
"""
Contains booking details for the entry, e.g. the end-to-end
identification and remittance information.
Description see section "Transaction Details (TxDtls, D-Level)".
"""

ADDITIONAL_ENTRY_INFORMATION = "AddtlNtryInf"
"""
This element may be used optionally by Swiss financial institutions
for further information at "Entry" level (e.g. for booking
information or to show charges which are not directly deducted
from the entry). This additional information always refers to the
relevant booking.
"""

RELATED_PARTIES = "RltdPties"
"""
Related parties, where known, can be shown on the statement.
Sub-elements as in the ISO standard. Below, those elements are
listed which are understood and delivered in the same way by
Swiss financial institutions.

In the case of R-transactions, the parties involved (Creditor/Debtor,
Ultimate Creditor/Ultimate Debtor) retain their roles from the
original transaction.
"""

CREDITOR = "Cdtr"
"""
Creditor.
"""

DEBITOR = "Dbtr"
"""
Debitor.
"""

REMITTANCE_INFORMATION = "RmtInf"
"""
The tag consists of a number of sub-elements. In Switzerland the
<CdtrRefInf> element can be filled in, where in the instruction the
structured "Creditor Reference" is given, e.g. ISR/QR/LSV reference,
IPI reference or the new international "Creditor’s Reference"
according to ISO 11649.
"""

UNSTRUCTURED = "Ustrd"
"""
This element can contain unstructured messages, e.g. for
messages from a "pain.001" instruction or booking information.
The element can occur more than once.
"""

STRUCTURED = "Strd"
"""
The tag consists of a number of sub-elements. In Switzerland the
<CdtrRefInf> element can be filled in, where in the instruction the
structured "Creditor Reference" is given, e.g. ISR/QR/LSV reference,
IPI reference or the new international "Creditor’s Reference"
according to ISO 11649.
"""

CREDITOR_REFERENCE_INFORMATION = "CdtrRefInf"
"""
In Switzerland the
<CdtrRefInf> element can be filled in, where in the instruction the
structured "Creditor Reference" is given, e.g. ISR/QR/LSV reference,
IPI reference or the new international "Creditor’s Reference"
according to ISO 11649.
"""

REFERENCE = "Ref"
"""Reference."""

CREDITOR_ACCOUNT = "CdtrAcct"
"""
Account of creditor.
"""

DEBITOR_ACCOUNT = "DbtrAcct"
"""
Account of debitor.
"""

OTHER = "Othr"
"""
Non standard account (postal account etc.).
"""
