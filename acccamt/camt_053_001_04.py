"""
Handles the import.
"""

from datetime import date, datetime
from decimal import Decimal
from enum import Enum
from pathlib import Path
from typing import Any, Dict, List, Optional

import pkg_resources
import xmlschema  # type: ignore

from acccamt import tags
from acccamt.codesets import Domain, PaymentFamily, PaymentSubFamily
from acccamt.exceptions import (InvalidAccountCodeError,
                                InvalidTransactionDomainCodeError,
                                InvalidReferenceTypeError,
                                InvalidTransactionFamilyCodeError,
                                InvalidTransactionKindCodeError,
                                InvalidTransactionSubFamilyCodeError)


class AccountType(str, Enum):
    """
    States the type of account.
    """
    IBAN = "iban"
    OTHER = "other"


class Camt053_001_04_Statement:
    """Represents a Bank statement."""

    __data: Dict[str, Any]

    def __init__(self, data: Dict[str, Any]) -> None:
        self.__data = data

    @classmethod
    def from_file(cls, path: Path) -> "Camt053_001_04_Statement":
        """Parses the content of a given XML file into a bank-statement."""
        schema = cls.load_schema()
        with open(path, "r") as raw:
            data = schema.to_dict(raw.read())
        return cls(data)

    @staticmethod
    def load_schema() -> xmlschema.XMLSchema:
        """Loads the XML Schema."""
        stream = pkg_resources.resource_stream(__name__, "camt.053.001.04.xsd")
        return xmlschema.XMLSchema(stream)

    @property
    def created_at(self) -> datetime:
        """Date and time, when the statement was created by the bank."""
        raw = (self.__data[tags.BANK_TO_CUSTOMER_STATEMENT]
                          [tags.GROUP_HEADER]
                          [tags.CREATION_DATE_TIME])
        return datetime.fromisoformat(raw)

    @property
    def account(self) -> str:
        """Account code (IBAN or other) of the account."""
        return _parse_account(self.__data[tags.BANK_TO_CUSTOMER_STATEMENT][
            tags.STATEMENT][0][tags.ACCOUNT])

    @property
    def account_owner(self) -> str:
        """Owner (company or person) of the account."""
        return (self.__data[tags.BANK_TO_CUSTOMER_STATEMENT][tags.STATEMENT][0]
                [tags.ACCOUNT][tags.OWNER][tags.NAME])

    @property
    def bank_name(self) -> str:
        """Name of the bank."""
        return (self.__data[tags.BANK_TO_CUSTOMER_STATEMENT][tags.STATEMENT][0]
                [tags.ACCOUNT][tags.SERVICER][
                    tags.FINANCIAL_INSTITUTION_IDENTIFICATION][tags.NAME])

    def transactions(self) -> List["Camt053_001_04_Transaction"]:
        """
        Returns a list of all transactions within the statement.
        """
        raw = (self.__data[tags.BANK_TO_CUSTOMER_STATEMENT][tags.STATEMENT][1][
            tags.ENTRY])
        return [Camt053_001_04_Transaction(t) for t in raw]


class TransactionKind(str, Enum):
    """
    States whether a transaction is debit or credit.
    """
    CREDIT = "credit"
    DEBIT = "debit"

    @classmethod
    def from_camt(cls, data: str) -> "TransactionKind":
        """
        Parses the camt codes.
        """
        if data == "CRDT":
            return cls.CREDIT
        if data == "DBIT":
            return cls.DEBIT
        raise InvalidTransactionKindCodeError(
            f'{data} is not a valid transaction kind code')


class Camt053_001_04_Transaction:
    """A transaction within a statement."""

    __data: Dict[str, Any]

    def __init__(self, data: Dict[str, Any]) -> None:
        self.__data = data

    @property
    def name(self) -> Optional[str]:
        """
        Returns the name of a transaction. Can be `None` as the
        `Additional Entry Information` is not mandatory.
        """
        if tags.ADDITIONAL_ENTRY_INFORMATION not in self.__data:
            return None
        return self.__data[tags.ADDITIONAL_ENTRY_INFORMATION]

    @property
    def amount(self) -> Decimal:
        """The amount of money."""
        return self.__data[tags.AMOUNT]["$"]

    @property
    def currency(self) -> str:
        """Currency code of the transaction."""
        return (self.__data[tags.AMOUNT][tags.CURRENCY])

    @property
    def kind(self) -> TransactionKind:
        """States whether it is a debit or credit transaction."""
        raw = self.__data[tags.CREDIT_DEBIT_INDICATOR]
        return TransactionKind.from_camt(raw)

    @property
    def transaction_domain(self) -> Domain:
        """
        Returns the `Domain` of the transaction.
        """
        code = (self.__data[tags.BANK_TRANSACTION_CODE]
                           [tags.DOMAIN]
                           [tags.CODE])
        domain = Domain.from_code(code)
        if domain is not None:
            return domain
        raise InvalidTransactionDomainCodeError()

    @property
    def transaction_family(self) -> PaymentFamily:
        """
        Returns the `PaymentFamily` of the transaction. Although the
        transaction domain can be determined by `transaction_domain` the
        package only implements the payment family and sub-family.
        """
        code = (self.__data[tags.BANK_TRANSACTION_CODE]
                           [tags.DOMAIN]
                           [tags.FAMILY]
                           [tags.CODE])
        family = PaymentFamily.from_code(code)
        if family is not None:
            return family
        raise InvalidTransactionFamilyCodeError()

    @property
    def transaction_sub_family(self) -> PaymentSubFamily:
        """
        Returns the `PaymentSubFamily` of the transaction. Although the
        transaction domain can be determined by `transaction_domain` the
        package only implements the payment family and sub-family. This
        property also supports generic sub-families.
        """
        code = (self.__data[tags.BANK_TRANSACTION_CODE]
                           [tags.DOMAIN]
                           [tags.FAMILY]
                           [tags.SUB_FAMILY_CODE])
        sub = PaymentSubFamily.from_code(code)
        if sub is not None:
            return sub
        raise InvalidTransactionSubFamilyCodeError()

    @property
    def value_date(self) -> date:
        """Returns the value date."""
        raw = self.__data[tags.VALUE_DATE][tags.DATE]
        return date.fromisoformat(raw)

    @property
    def creditor_name(self) -> str:
        """Name of the creditor."""
        return (self.__data[tags.ENTRY_DETAILS][0]
                           [tags.TRANSACTION_DETAILS][0]
                           [tags.RELATED_PARTIES]
                           [tags.CREDITOR]
                           [tags.NAME])

    @property
    def creditor_address(self) -> Optional[List[str]]:
        """Postal address of the creditor if available."""
        creditor = (self.__data[tags.ENTRY_DETAILS][0][
            tags.TRANSACTION_DETAILS][0][tags.RELATED_PARTIES][tags.CREDITOR])
        return _parse_address(creditor)

    @property
    def creditor_account(self) -> str:
        """Account code (e.g. IBAN) of the creditor."""
        return _parse_account(
            self.__data[tags.ENTRY_DETAILS][0][tags.TRANSACTION_DETAILS][0][
                tags.RELATED_PARTIES][tags.CREDITOR_ACCOUNT])

    @property
    def debitor_name(self) -> str:
        """Name of the debitor."""
        return (self.__data[tags.ENTRY_DETAILS][0][tags.TRANSACTION_DETAILS][0]
                [tags.RELATED_PARTIES][tags.DEBITOR][tags.NAME])

    @property
    def debitor_address(self) -> Optional[List[str]]:
        """Postal address of the debitor if available."""
        debitor = (self.__data[tags.ENTRY_DETAILS][0][tags.TRANSACTION_DETAILS]
                   [0][tags.RELATED_PARTIES][tags.DEBITOR])
        return _parse_address(debitor)

    @property
    def debitor_account(self) -> str:
        """Account code (e.g. IBAN) of the debitor."""
        return _parse_account(
            self.__data[tags.ENTRY_DETAILS][0][tags.TRANSACTION_DETAILS][0][
                tags.RELATED_PARTIES][tags.DEBITOR_ACCOUNT])

    @property
    def reference(self) -> Optional[str]:
        """Reference of the transaction."""
        details = (
            self.__data[tags.ENTRY_DETAILS][0][tags.TRANSACTION_DETAILS][0])
        if tags.REMITTANCE_INFORMATION not in details:
            return None

        info = details[tags.REMITTANCE_INFORMATION]
        if tags.STRUCTURED in info:
            return (info[tags.STRUCTURED][0][
                tags.CREDITOR_REFERENCE_INFORMATION][tags.REFERENCE])
        if tags.UNSTRUCTURED in info:
            return " ".join(info[tags.UNSTRUCTURED])
        raise InvalidReferenceTypeError()

    def __str__(self) -> str:
        return f"name: '{self.name}', amount: {self.amount}, "\
           f"currency: {self.currency}, kind: {self.kind}, "\
           f"family: {self.transaction_family}, "\
           f"sub-family: {self.transaction_sub_family}, "\
           f"value-date: {self.value_date}, "\
           f"creditor: {self.creditor_name}, debitor: {self.debitor_name}, "\
           f"reference: {self.reference}"


def _parse_address(data: Dict[str, Any]) -> Optional[List[str]]:
    if tags.POSTAL_ADDRESS not in data:
        return None
    address = data[tags.POSTAL_ADDRESS]
    if tags.ADDRESS_LINE in address:
        return address[tags.ADDRESS_LINE]
    return [
        address[tags.STREET_NAME] or "?",
        address[tags.BUILDING_NUMBER] or "?",
        address[tags.POST_CODE] or "?",
        address[tags.TOWN_NAME] or "?",
    ]


def _parse_account(data: Dict[str, Any]) -> str:
    account = data[tags.IDENTIFICATION]
    if tags.IBAN in account:
        return account[tags.IBAN]
    if tags.OTHER in account:
        return (account[tags.OTHER][tags.IDENTIFICATION])
    raise InvalidAccountCodeError()
