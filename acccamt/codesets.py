"""
Module contains the handling of External Code sets.

Attention: This module doesn't support all possible codes; it aims to support
the full `Payments` domain as well as account management codes.
"""

from enum import Enum
from typing import Dict, Optional


class Domain(str, Enum):
    """
    The different domains of external code set
    (ExternalBankTransactionDomain1Code).
    """

    PAYMENTS = 'payments'
    CASH_MANAGEMENT = 'cash_management'
    DERIVATIVES = 'derivatives'
    LOANS_DEPOSITS_SYNDICATIONS = 'loans_deposits_syndications'
    FOREIGN_EXCHANGE = 'foreign_exchange'
    PRECIOUS_METAL = 'precious_metal'
    COMMODITIES = 'commodities'
    TRADE_SERVICES = 'trade_services'
    SECURITIES = 'securities'
    ACCOUNT_MANAGEMENT = 'account_management'
    EXTENDED_DOMAIN = 'extended_domain'

    @classmethod
    def from_code(cls, code: str) -> Optional["Domain"]:
        """
        Returns the `DomainCode` for a given code. Returns `None` if no
        is defined.
        """
        codes: Dict[str, "Domain"] = {
            'PMNT': cls.PAYMENTS,
            'CAMT': cls.CASH_MANAGEMENT,
            'DERV': cls.DERIVATIVES,
            'LDAS': cls.LOANS_DEPOSITS_SYNDICATIONS,
            'FORX': cls.FOREIGN_EXCHANGE,
            'PMET': cls.PRECIOUS_METAL,
            'CMDT': cls.COMMODITIES,
            'TRAD': cls.TRADE_SERVICES,
            'SECU': cls.SECURITIES,
            'ACMT': cls.ACCOUNT_MANAGEMENT,
            'XTND': cls.EXTENDED_DOMAIN,
        }
        if code not in codes:
            return None
        return codes[code]


class PaymentFamily(str, Enum):
    """
    The families of Bank Transaction Codes for Switzerland as defined in
    Appendix B, Swiss Implementation Guidelines.
    """

    COUNTER_TRANSACTION = "counter_transaction"
    CUSTOMER_CARD_TRANSACTION = "customer_card_transaction"
    ISSUED_CHEQUES = "issued_cheques"
    ISSUED_CREDIT_TRANSFERS = "issued_credit_transfers"
    ISSUED_DIRECT_DEBITS = "issued_direct_debits"
    RECEIVED_CHEQUES = "received_cheques"
    RECEIVED_CREDIT_TRANSFERS = "received_credit_transfers"
    RECEIVED_DIRECT_DEBITS = "received_direct_debits"
    NOT_AVAILABLE = 'not_available'
    OTHER = 'other'
    MISCELLANEOUS_CREDIT_OPERATIONS = 'miscellaneous_credit_operations'
    MISCELLANEOUS_DEBIT_OPERATIONS = 'miscellaneous_debit_operations'

    @classmethod
    def from_code(cls, code: str) -> Optional["PaymentFamily"]:
        """
        Returns the `PaymentFamily` for a given code. Returns `None` if no
        is defined.
        """
        codes: Dict[str, "PaymentFamily"] = {
            "CNTR": cls.COUNTER_TRANSACTION,
            "CCRD": cls.CUSTOMER_CARD_TRANSACTION,
            "ICHQ": cls.ISSUED_CHEQUES,
            "ICDT": cls.ISSUED_CREDIT_TRANSFERS,
            "IDDT": cls.ISSUED_DIRECT_DEBITS,
            "RCHQ": cls.RECEIVED_CHEQUES,
            "RCDT": cls.RECEIVED_CREDIT_TRANSFERS,
            "RDDT": cls.RECEIVED_DIRECT_DEBITS,
            'NTAV': cls.NOT_AVAILABLE,
            'OTHR': cls.OTHER,
            'MCOP': cls.MISCELLANEOUS_CREDIT_OPERATIONS,
            'MDOP': cls.MISCELLANEOUS_DEBIT_OPERATIONS,
        }
        if code not in codes:
            return None
        return codes[code]


class PaymentSubFamily(str, Enum):
    """
    The sub-families of Bank Transaction Codes for Switzerland as defined in
    Appendix B, Swiss Implementation Guidelines.

    It also contains the sub-families of generic families.
    """
    CASH_DEPOSIT = "cash_deposit"
    CASH_WITHDRAWAL = "cash_withdrawal"
    CHECK_DEPOSIT = "check_deposit"
    CROSS_BORDER_CASH_WITHDRAWAL = "cross_border_cash_withdrawal"
    POINT_OF_SALE_PAYMENT_DEBIT_CARD = "point_of_sale_payment_debit_card"
    CHEQUE = "cheque"
    FOREIGN_CHEQUE = "foreign_cheque"
    AUTOMATIC_TRANSFER = "automatic_transfer"
    DOMESTIC_CREDIT_TRANSFER = "domestic_credit_transfer"
    CREDIT_TRANSFER_WITH_AGREED_COMMERCIAL_INFORMATION = \
        "credit_transfer_with_agreed_commercial_information"
    INTERNAL_BOOK_TRANSFER = "internal_book_transfer"
    PAYROLL_SALARY_PAYMENT = "payroll_salary_payment"
    PRIORITY_CREDIT_TRANSFER = "priority_credit_transfer"
    REVERSAL_DUE_TO_PAYMENT_RETURN = "reversal_due_to_payment_return"
    SEPA_CREDIT_TRANSFER = "sepa_credit_transfer"
    STANDING_ORDER = "standing_order"
    CROSS_BORDER_DIRECT_DEBIT = "cross_border_direct_debit"
    DIRECT_DEBIT = "direct_debit"
    REVERSAL_DUE_TO_PAYMENT_CANCELLATION_REQUEST = \
        "reversal_due_to_payment_cancellation_request"
    REVERSAL_DUE_TO_RETURN_UNPAID_DIRECT_DEBIT = \
        "reversal_due_to_return_unpaid_direct_debit"
    REVERSAL_DUE_TO_PAYMENT_REVERSAL = "reversal_due_to_payment_reversal"
    SEPA_B2B_DIRECT_DEBIT = "sepa_b2b_direct_debit"
    SEPA_CORE_DIRECT_DEBIT = "sepa_core_direct_debit"
    CHEQUE_REVERSAL = "cheque_reversal"
    CHEQUE_UNDER_RESERVE = "cheque_under_reserve"
    ACH_TRANSACTION = "ach_transaction"
    CROSS_BORDER_CREDIT_TRANSFER = "cross_border_credit_transfer"
    FEES = 'fees'
    COMMISSION = 'commission'
    COMMISSION_EXCLUDING_TAXES = 'commission_excluding_taxes'
    COMMISSION_INCLUDING_TAXES = 'commission_including_taxes'
    NON_TAXABLE_COMMISSIONS = 'non_taxable_commissions'
    TAXES = 'taxes'
    CHARGES = 'charges'
    INTEREST = 'interest'
    REIMBURSEMENTS = 'reimbursements'
    ADJUSTMENTS = 'adjustments'
    NOT_AVAILABLE = 'not_available'
    OTHER = 'other'

    @classmethod
    def from_code(cls, code: str) -> Optional["PaymentSubFamily"]:
        """
        Returns the `PaymentSubFamily` for a given code. Returns `None` if
        no is defined.
        """
        codes: Dict[str, PaymentSubFamily] = {
            "CDPT": cls.CASH_DEPOSIT,
            "CWDL": cls.CASH_WITHDRAWAL,
            "CHKD": cls.CHECK_DEPOSIT,
            "XBCW": cls.CROSS_BORDER_CASH_WITHDRAWAL,
            "POSD": cls.POINT_OF_SALE_PAYMENT_DEBIT_CARD,
            "CCHQ": cls.CHEQUE,
            "XBCQ": cls.FOREIGN_CHEQUE,
            "AUTT": cls.AUTOMATIC_TRANSFER,
            "DMCT": cls.DOMESTIC_CREDIT_TRANSFER,
            "VCOM": cls.CREDIT_TRANSFER_WITH_AGREED_COMMERCIAL_INFORMATION,
            "BOOK": cls.INTERNAL_BOOK_TRANSFER,
            "SALA": cls.PAYROLL_SALARY_PAYMENT,
            "PRCT": cls.PRIORITY_CREDIT_TRANSFER,
            "RRTN": cls.REVERSAL_DUE_TO_PAYMENT_RETURN,
            "ESCT": cls.SEPA_CREDIT_TRANSFER,
            "STDO": cls.STANDING_ORDER,
            "XBDD": cls.CROSS_BORDER_DIRECT_DEBIT,
            "PMDD": cls.DIRECT_DEBIT,
            "RCDD": cls.REVERSAL_DUE_TO_PAYMENT_CANCELLATION_REQUEST,
            "UPDD": cls.REVERSAL_DUE_TO_RETURN_UNPAID_DIRECT_DEBIT,
            "PRDD": cls.REVERSAL_DUE_TO_PAYMENT_REVERSAL,
            "BBDD": cls.SEPA_B2B_DIRECT_DEBIT,
            "ESDD": cls.SEPA_CORE_DIRECT_DEBIT,
            "CQRV": cls.CHEQUE_REVERSAL,
            "URCQ": cls.CHEQUE_UNDER_RESERVE,
            "AXTN": cls.AUTOMATIC_TRANSFER,
            "XBCT": cls.CROSS_BORDER_CREDIT_TRANSFER,
            'FEES': cls.FEES,
            'COMM': cls.COMMISSION,
            'COME': cls.COMMISSION_EXCLUDING_TAXES,
            'COMI': cls.COMMISSION_INCLUDING_TAXES,
            'COMT': cls.NON_TAXABLE_COMMISSIONS,
            'TAXE': cls.TAXES,
            'CHRG': cls.CHARGES,
            'INTR': cls.INTEREST,
            'RIMB': cls.REIMBURSEMENTS,
            'ADJT': cls.ADJUSTMENTS,
            'NTAV': cls.NOT_AVAILABLE,
            'OTHR': cls.OTHER,
        }
        if code not in codes:
            return None
        return codes[code]
