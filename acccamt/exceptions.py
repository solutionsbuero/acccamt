"""
This module contains the exceptions for the camt import.
"""


class InvalidAccountCodeError(Exception):
    """
    Raised when structure of a account code is unknown.
    (E.g. not `IBAN` or `OTHER`.)
    """
    pass


class InvalidTransactionDomainCodeError(Exception):
    """
    Raised when no TransactionDomain is found for a given code.
    """
    pass


class InvalidTransactionFamilyCodeError(Exception):
    """
    Raised when no TransactionFamily is found for a given code.
    """
    pass


class InvalidTransactionSubFamilyCodeError(Exception):
    """
    Raised when no TransactionSubFamily is found for a given code.
    """
    pass


class InvalidReferenceTypeError(Exception):
    """
    Raised when structure for a given reference is unknown.
    """
    pass


class InvalidTransactionKindCodeError(Exception):
    """
    Raised when a invalid code is given by the XML input.
    """
    pass
